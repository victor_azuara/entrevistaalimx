
const Avatar = () => {
  return (
    <div>
      <img
        src="https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg"
        className="rounded-circle img-thumbnail w-100"
        alt="Avatar"
      />
    </div>
  )
}

export default Avatar;