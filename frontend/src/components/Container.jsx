import Header from './Header';
import Content from './Content';

const Container = () => {
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="p-0 col-md-6 col-lg-4 border rounded">
          <Header />
          <Content />
        </div>
      </div>
    </div>
  )
}

export default Container;