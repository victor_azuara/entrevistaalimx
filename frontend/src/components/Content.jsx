import { useMutation } from "@apollo/client";
import { ADD_RECORD } from "../graphql/Mutations";
import { useStore } from './../store/StoreProvider';
import Message from "./Message";

const Content = () => {
  const { personal, birthday, contact } = useStore();
  const [addRecord, { loading, error, data } ] = useMutation(ADD_RECORD)

  if (error) console.log('Error :(', error);
  if (data) {
    localStorage.setItem('personal', JSON.stringify(personal));
    localStorage.setItem('birthday', JSON.stringify(birthday));
    localStorage.setItem('contact', JSON.stringify(contact));
  }

  const add = () => {
    addRecord({
      variables: {
        input: {
          firstname: personal.firstname,
          secondname: personal.secondname,
          lastname: personal.lastname,
          motherlastname: personal.motherlastname,
          birthday: `${birthday.day} ${birthday.month} ${birthday.year}`,
          email: contact.email,
          phone: contact.phone
        }
      }
    })
  }
  
  return (
    <div style={{ overflowY: 'scroll', maxHeight: '75vh' }}>
      <Message comp='personal' />

      { Object.values(personal).every(x => x !== '') ? <Message comp='birth' /> : null }
      
      {
        (Object.values(personal).every(x => x !== '') && Object.values(birthday).every(x => x !== ''))
          ? <Message comp='contact' /> : null
      }
      
      <div className="d-grid mx-2 my-2" >
        {
          (Object.values(personal).every(x => x !== '') &&
          Object.values(birthday).every(x => x !== '') &&
          Object.values(contact).every(x => x !== ''))
            ? 
            <button type="button" className="btn btn-pink" onClick={add} disabled={ loading }>
              { loading ? 'Cargando...' : 'Iniciar' }
            </button>
            : null
        }
      </div>
      { data ? <div className="p-2 m-2 border rounded bg-pink">Sus datos se guardaron correctamente</div> : null }
    </div>
  )
}

export default Content;