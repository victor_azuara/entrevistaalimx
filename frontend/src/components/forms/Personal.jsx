import { Fragment } from 'react'
import { types } from '../../store/StoreReducer';
import { useDispatch, useStore } from './../../store/StoreProvider';

const Personal = () => {
  const { personal } = useStore();
  const dispatch = useDispatch();

  const handlerInputChange = event => {
    dispatch({
      type: types.personalChange,
      payload: { ...personal, [event.target.name] : event.target.value }
    });
  }

  return (
    <Fragment>
      <div className="container border rounded bg-light">
        <h5 className="my-4">¿Cuál es tu nombre?</h5>
        <div className="mb-3">
          <input
             className="form-control form-control-sm"
             type="text"
             name="firstname"
             placeholder="Nombre"
             onBlur={handlerInputChange}
             />
        </div>
        <div className="mb-3">
          <input
             className="form-control form-control-sm"
             type="text"
             name="secondname"
             placeholder="Segundo nombre"
             onBlur={handlerInputChange}
             />
        </div>
        <div className="mb-3">
          <input
             className="form-control form-control-sm"
             type="text"
             name="lastname"
             placeholder="Apellido paterno"
             onBlur={handlerInputChange}
             />
        </div>
        <div className="mb-3">
          <input
             className="form-control form-control-sm"
             type="text"
             name="motherlastname"
             placeholder="Apellido materno"
             onBlur={handlerInputChange}
           />
        </div>
      </div>
      {
        Object.values(personal).every(x => x !== '')
          ? <div className="p-2 my-2 border rounded bg-pink">
            {personal.firstname} {personal.secondname} {personal.lastname} {personal.lastmothername}
          </div>
          : null
      }
    </Fragment>
  )
}

export default Personal;