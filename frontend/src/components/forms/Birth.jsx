import { Fragment } from 'react'
import { types } from '../../store/StoreReducer';
import { useDispatch, useStore } from './../../store/StoreProvider';

const Birth = () => {
  const { birthday } = useStore();
  const dispatch = useDispatch();

  const handlerInputChange = event => {
    dispatch({
      type: types.birthdayChange,
      payload: { ...birthday, [event.target.name] : event.target.value }
    });
  }

  return (
    <Fragment>
      <div className="container border rounded bg-light">
        <h5 className="my-4">¿Cuál es tu fecha de nacimiento?</h5>
        <div className="mb-3">
          <input
            className="form-control form-control-sm"
            type="text"
            placeholder="Día"
            name="day"
            onBlur={handlerInputChange}
          />
        </div>
        <div className="mb-3">
          <input
            className="form-control form-control-sm"
            type="text"
            name="month"
            placeholder="Mes"
            onBlur={handlerInputChange}
          />
        </div>
        <div className="mb-3">
          <input
            className="form-control form-control-sm"
            type="text"
            name="year"
            placeholder="Año"
            onBlur={handlerInputChange}
          />
        </div>
      </div>
      {
        Object.values(birthday).every(x => x !== '')
          ? <div className="p-2 my-2 border rounded bg-pink">
            {birthday.day} {birthday.month} {birthday.year}
          </div>
          : null
      }
    </Fragment>
  )
}

export default Birth;