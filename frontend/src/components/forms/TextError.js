
const TextError = (props) => {
  return (
    <small className="text-danger">
      { props.children }
    </small>
  )
}

export default TextError
