import { Fragment } from 'react'
import { types } from '../../store/StoreReducer';
import { useDispatch, useStore } from './../../store/StoreProvider';

const Contact = () => {
  const { contact } = useStore();
  const dispatch = useDispatch();
  
  const handlerInputChange = event => {
    dispatch({
      type: types.contactChange,
      payload: { ...contact, [event.target.name] : event.target.value }
    });
  }

  return (
    <Fragment>
      <div className="container border rounded bg-light">
        <h5 className="my-4">Datos de contacto</h5>
        <div className="mb-3">
          <input
            className="form-control form-control-sm"
            type="email"
            name="email"
            placeholder="Correo electrónico"
            onBlur={handlerInputChange}
          />
        </div>
        <div className="mb-3">
          <input
            className="form-control form-control-sm"
            type="text"
            name="phone"
            placeholder="Teléfono celular"
            onBlur={handlerInputChange}
          />
        </div>
      </div>
      {
        Object.values(contact).every(x => x !== '')
          ? <div className="p-2 my-2 border rounded bg-pink">
              <div>Correo: {contact.email}</div>
              <div>Celular: {contact.phone}</div>
          </div>
          : null
      }
    </Fragment>
  )
}

export default Contact;