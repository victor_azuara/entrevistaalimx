import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClipboardList, faStopwatch } from '@fortawesome/free-solid-svg-icons'

const Header = () => {
  return (
    <div className="bg-pink border-bottom border-pink border-5">
      <div className="d-flex flex-row justify-content-around align-items-start pt-4">
        <h5>Titulo del Formulario</h5>
        <FontAwesomeIcon icon={faClipboardList} size="4x" inverse/>
      </div>
      <div className="px-4 py-3">
        <FontAwesomeIcon icon={faStopwatch} size="lg" />
        <span className="mx-3">En menos de 5 minutos</span>
      </div>
    </div>
  )
}

export default Header;