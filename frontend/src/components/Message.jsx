import { Fragment, React } from 'react';
import Avatar from "./avatar";
import Personal from "./forms/Personal";
import Birth from "./forms/Birth";
import Contact from "./forms/Contact";

const Message = (props) => {
  return (
    <Fragment>
      <div className="d-flex justify-content-around my-4 mx-2">
        <div className="col-2 mx-2">
          <Avatar />
        </div>
        <div>
          { props.comp === 'personal' ? <Personal />: null }
          { props.comp === 'birth' ? <Birth />: null }
          { props.comp === 'contact' ? <Contact />: null }
        </div>
      </div>
    </Fragment>
  )
}

export default Message;