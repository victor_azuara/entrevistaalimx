const types = {
  personalChange: 'personal - change',
  birthdayChange: 'birthday - change',
  contactChange: 'contact - change'
}

const initialStore = {
  personal: {
    firstname: '',
    secondname: '',
    lastname: '',
    motherlastname: ''
  },
  birthday: {
    day: '',
    month: '',
    year: ''
  },
  contact: {
    email: '',
    phone: ''
  }
}

const storeReducer = (state, action) => {
  console.log(action)
  switch(action.type) {
    case types.personalChange:
      return {
        ...state,
        personal: {
          firstname: action.payload.firstname,
          secondname: action.payload.secondname,
          lastname: action.payload.lastname,
          motherlastname: action.payload.motherlastname
        }
      }
    case types.birthdayChange:
      return {
        ...state,
        birthday: {
          day: action.payload.day,
          month: action.payload.month,
          year: action.payload.year
        }
      }
    case types.contactChange:
      return {
        ...state,
        contact: {
          email: action.payload.email,
          phone: action.payload.phone
        }
      }
    default:
        return state;
  }
}

export { initialStore, types }
export default storeReducer;