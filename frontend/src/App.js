import Container from './components/Container';

import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faCheckSquare, faCoffee } from '@fortawesome/free-solid-svg-icons'
import StoreProvider from './store/StoreProvider';

library.add(fab, faCheckSquare, faCoffee)

function App() {

  return (
    <div>
      <StoreProvider>
        <Container />
      </StoreProvider>
    </div>
  );
}

export default App;
