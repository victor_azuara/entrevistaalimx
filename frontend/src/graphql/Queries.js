import { gql } from "@apollo/client";

export const LOAD_RECORDS = gql`
  query GetRecords {
    getRecords {
      firstname
      secondname
      lastname
      motherlastname
      birthday
      email
      phone
    }
  }
`;