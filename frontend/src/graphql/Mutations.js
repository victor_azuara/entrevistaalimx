import { gql } from "@apollo/client";

export const ADD_RECORD = gql`
  mutation($input: AddInput){
    addRegister(addInput: $input)
  }
`;