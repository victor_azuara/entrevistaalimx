import { conn } from './../mysql/conection'; 
import { promisify } from 'util';

const query = promisify(conn.query).bind(conn);

const resolvers = {
  Query: {
    async getRecords() {
      const q = 'SELECT * FROM users_test_victorazuara'
      try {
        const res = await query(q);
        return res;
      } catch (error) {
        return 'Operacion fallida ;(';
      }
    }
  },
  Mutation: {
    async addRegister(_, {addInput}) {
      const { firstname, secondname, lastname, motherlastname, birthday, email, phone } = addInput;
      const q = `INSERT INTO users_test_victorazuara (firstname, secondname, lastname, motherlastname, birthday, email, phone) VALUES (
        '${firstname}','${secondname}','${lastname}','${motherlastname}','${birthday}','${email}', '${phone}'
      )`;

      try {
        const res = await query(q);
        return res.affectedRows;
      } catch (error) {
        return 'Operacion fallida ;(';
      }
    }
  }
};

export default resolvers;