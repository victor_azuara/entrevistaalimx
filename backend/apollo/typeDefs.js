import { gql } from 'apollo-server';

const typeDefs = gql`
type Registro {
  firstname: String,
  secondname: String,
  lastname: String,
  motherlastname: String,
  birthday: String,
  email: String,
  phone: String
}

input AddInput {
  firstname: String,
  secondname: String,
  lastname: String,
  motherlastname: String,
  birthday: String,
  email: String,
  phone: String
}

type Query {
  getRecords: [Registro]
}

type Mutation {
  addRegister(addInput: AddInput): String
}
`;

export default typeDefs;