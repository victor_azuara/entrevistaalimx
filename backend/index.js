import { ApolloServer } from 'apollo-server';
import { conn } from './mysql/conection';
import typeDefs from './apollo/typeDefs';
import resolvers from './apollo/resolvers';

conn.connect((error) => {
  if (error) {
    throw error;
  } else {
    console.log('Successful Connection');
  }
});

const server = new ApolloServer({
  cors: {
		origin: '*',
    credentials: true
  },
  typeDefs,
  resolvers
});

server.listen().then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
